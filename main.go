package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"test-goroutines-performance/worker"
)

var numbersCount = 10_000_000
var operationsRepeats = 1000

func main() {
	poll()

	// Each test case is count of goroutines that will work in each iteration
	testCases := []int{
		1,
		50,
		100,
		150,
		200,
		500,
		1_000,
		10_000,
		100_000,
	}

	w := worker.New(operationsRepeats)
	var results []worker.WorkResult
	for _, testCase := range testCases {
		result := w.Work(generateNumbers(numbersCount), testCase)
		results = append(results, result)
		fmt.Printf("Done test case %d\n", testCase)
	}

	report(results...)
}

func poll() {
	s := bufio.NewScanner(os.Stdin)
	fmt.Printf("Type numbers count to generate for test (int) default is %d:\n", numbersCount)
	s.Scan()
	numbersCountRaw := s.Text()
	numbersCountConv, err := strconv.Atoi(numbersCountRaw)
	if err != nil {
		log.Fatal("Wrong type")
	}
	numbersCount = numbersCountConv
	fmt.Println(numbersCountRaw)
}

func generateNumbers(count int) []int {
	var result []int
	for i := 0; i < count; i++ {
		result = append(result, int(rand.Uint32()))
	}
	return result
}

func report(result ...worker.WorkResult) {
	br := "----------"
	numbers := fmt.Sprintf("%d numbers", numbersCount)
	repeatsForEachOperation := fmt.Sprintf("%d repeats for each operation", operationsRepeats)
	totalOperations := fmt.Sprintf("%d total operations", numbersCount*operationsRepeats)

	prints := []string{
		numbers,
		repeatsForEachOperation,
		totalOperations,
		br,
	}

	for _, r := range result {
		title := fmt.Sprintf("%d goroutines\n", r.GoroutinesCount)
		prints = append(prints, title)
		prints = append(prints, r.Duration)
		prints = append(prints, br)
	}

	for _, p := range prints {
		fmt.Println(p)
	}
}
