package worker

import (
	"sync"
	"time"
)

type WorkResult struct {
	GoroutinesCount int
	Duration        string
}

type Worker struct {
	numbers           []int
	numbersCount      int
	operationsRepeats int
}

func New(operationsRepeats int) Worker {
	return Worker{
		operationsRepeats: operationsRepeats,
	}
}

func (w *Worker) Work(numbers []int, goroutinesCount int) WorkResult {
	if w.operationsRepeats == 0 {
		panic("No operationsRepeats passed. Use worker.New(int)")
	}

	w.numbers = numbers
	w.numbersCount = len(numbers)

	start := time.Now()

	wg := sync.WaitGroup{}
	w.batch(w.numbers, &wg, goroutinesCount)
	wg.Wait()

	duration := time.Since(start).String()

	return WorkResult{
		GoroutinesCount: goroutinesCount,
		Duration:        duration,
	}
}

func (w *Worker) batch(numbers []int, wg *sync.WaitGroup, batchesCount int) {
	perBatchCount := w.numbersCount / batchesCount

	for i := 0; i < batchesCount; i++ {
		start := i * perBatchCount
		end := start + perBatchCount - 1
		wg.Add(1)
		go func() {
			defer wg.Done()
			for x := start; x < end; x++ {
				numbers[x] = w.operate(numbers[x])
			}
		}()
	}
}

func (w *Worker) operate(v int) int {
	result := v
	for i := 0; i < w.operationsRepeats; i++ {
		result = v * v
	}
	return result
}
